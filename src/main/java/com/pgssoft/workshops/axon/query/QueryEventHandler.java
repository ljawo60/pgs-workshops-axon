package com.pgssoft.workshops.axon.query;

import com.pgssoft.workshops.axon.query.model.Account;
import com.pgssoft.workshops.axon.query.service.AccountService;
import com.pgssoft.workshops.axon.shared.event.AccountCreatedEvent;
import com.pgssoft.workshops.axon.shared.event.AccountModifiedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class QueryEventHandler {

    private final AccountService accountService;

    @EventHandler
    public void on(AccountCreatedEvent accountCreatedEvent) {
        log.info("on(accountCreatedEvent={}", accountCreatedEvent);
        accountService.save(new Account(accountCreatedEvent.getId(), accountCreatedEvent.getEmail()));
    }

    @EventHandler
    public void on(AccountModifiedEvent accountModifiedEvent) {
        log.info("on(accountModifiedEvent={}", accountModifiedEvent);
        Account account = accountService.get(accountModifiedEvent.getId());
        account.setEmail(accountModifiedEvent.getEmail());
    }
}

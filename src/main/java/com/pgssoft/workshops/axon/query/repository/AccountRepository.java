package com.pgssoft.workshops.axon.query.repository;

import com.pgssoft.workshops.axon.query.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

}
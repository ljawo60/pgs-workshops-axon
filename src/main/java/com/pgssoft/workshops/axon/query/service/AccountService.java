package com.pgssoft.workshops.axon.query.service;

import com.pgssoft.workshops.axon.query.model.Account;
import com.pgssoft.workshops.axon.query.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;

    public List<Account> getAll() {
        log.debug("getAll()");
        return accountRepository.findAll();
    }

    public Account save(Account account) {
        log.debug("save(account={})", account);
        return accountRepository.save(account);
    }

    public Account get(String id) {
        log.debug("get(id={})", id);
        return accountRepository.getOne(id);
    }

}

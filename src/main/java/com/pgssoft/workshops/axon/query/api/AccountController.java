package com.pgssoft.workshops.axon.query.api;

import com.pgssoft.workshops.axon.query.model.Account;
import com.pgssoft.workshops.axon.query.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/accounts")
@AllArgsConstructor
@Slf4j
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public List<Account> getAll() {
        log.debug("getAll()");
        return accountService.getAll();
    }

}

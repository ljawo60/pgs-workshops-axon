package com.pgssoft.workshops.axon.kafka;


import com.pgssoft.workshops.axon.shared.event.AccountCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
@ProcessingGroup("kafkaEventHandler")
@Slf4j
public class KafkaEventHandler {

    @EventHandler
    public void handleMyEvent(AccountCreatedEvent myEvent) {
        log.info("Received an event from the distributed event bus {}", myEvent);
    }

}
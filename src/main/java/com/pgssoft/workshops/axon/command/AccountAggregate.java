package com.pgssoft.workshops.axon.command;

import com.pgssoft.workshops.axon.shared.command.CreateAccountCommand;
import com.pgssoft.workshops.axon.shared.command.ModifyAccountCommand;
import com.pgssoft.workshops.axon.shared.event.AccountCreatedEvent;
import com.pgssoft.workshops.axon.shared.event.AccountModifiedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.Objects;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
@Slf4j
public class AccountAggregate {

    @AggregateIdentifier
    private String id;
    private String email;

    public AccountAggregate() {
    }

    @CommandHandler
    public AccountAggregate(CreateAccountCommand command) {
        log.info("AccountAggregate(command={}", command);
        Objects.requireNonNull(command.getAccountId());
        Objects.requireNonNull(command.getEmail());

        apply(new AccountCreatedEvent(command.getAccountId(), command.getEmail()));
    }

    @EventHandler
    public void on(AccountCreatedEvent accountCreatedEvent) {
        log.info("on(accountCreatedEvent={}", accountCreatedEvent);
        this.id = accountCreatedEvent.getId();
        this.email = accountCreatedEvent.getEmail();
    }

    @CommandHandler
    public void on(ModifyAccountCommand command) {
        log.info("ModifyAccountCommand(command={}", command);
        Objects.requireNonNull(command.getAccountId());
        Objects.requireNonNull(command.getEmail());

        apply(new AccountModifiedEvent(command.getAccountId(), command.getEmail()));
    }

    @EventHandler
    public void on(AccountModifiedEvent accountModifiedEvent) {
        log.info("on(accountModifiedEvent={}", accountModifiedEvent);
        this.email = accountModifiedEvent.getEmail();
    }
}

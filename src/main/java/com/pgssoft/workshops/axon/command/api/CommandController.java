package com.pgssoft.workshops.axon.command.api;

import com.pgssoft.workshops.axon.query.model.Account;
import com.pgssoft.workshops.axon.shared.command.CreateAccountCommand;
import com.pgssoft.workshops.axon.shared.command.ModifyAccountCommand;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/accounts")
@AllArgsConstructor
@Slf4j
public class CommandController {

    private final CommandGateway commandGateway;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String save(@RequestBody Account account) {
        log.debug("save(account={})", account);
        String id = UUID.randomUUID().toString();
        return commandGateway.sendAndWait(new CreateAccountCommand(id, account.getEmail()));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public String replace(@RequestBody Account account) {
        log.debug("replace(account={})", account);
        return commandGateway.sendAndWait(new ModifyAccountCommand(account.getId(), account.getEmail()));
    }
}

package com.pgssoft.workshops.axon.shared.command;

import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

@Value
public class ModifyAccountCommand {

    @TargetAggregateIdentifier
    private String accountId;
    private String email;

}

package com.pgssoft.workshops.axon.shared.event;

import lombok.Value;

@Value
public class AccountModifiedEvent {

    private String id;
    private String email;

}

package com.pgssoft.workshops.axon.shared.command;

import lombok.Value;

@Value
public class CreateAccountCommand {

    private String accountId;
    private String email;

}

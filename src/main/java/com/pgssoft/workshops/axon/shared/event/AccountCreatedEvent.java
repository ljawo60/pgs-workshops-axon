package com.pgssoft.workshops.axon.shared.event;

import lombok.Value;

@Value
public class AccountCreatedEvent {

    private String id;
    private String email;

}
